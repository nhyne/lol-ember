import DS from 'ember-data';

export default DS.Model.extend({
  games: DS.hasMany('game'),
  winner: DS.attr('boolean'),
  redKills: DS.attr('number'),
  blueKills: DS.attr('number')
});
