import DS from 'ember-data';

export default DS.Model.extend({
  games: DS.hasMany('game'),
  summonerName: DS.attr('string')
});
