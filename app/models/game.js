import DS from 'ember-data';

export default DS.Model.extend({
  match: DS.belongsTo('match'),
  player: DS.belongsTo('player'),

  kills: DS.attr('number'),
  deaths: DS.attr('number'),
  assists: DS.attr('number'),
  championId: DS.attr('number')
});
