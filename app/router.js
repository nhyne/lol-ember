import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('players', function() {
    this.route('show', { path: '/:id' });
  });
  this.route('matches', function() {
    this.route('show', { path: '/:id' }),
    this.route('index', { path: '/' });
  });
});

export default Router;
