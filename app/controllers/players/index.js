import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default Controller.extend({
  players: computed(function players() {
    return this.get('model');
  })
});
