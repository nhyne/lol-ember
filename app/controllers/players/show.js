import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default Controller.extend({
  player: computed(function player() {
    return this.get('model');
  })
});
