import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default Controller.extend({
  matches: computed(function matches() {
    return this.get('model');
  })
});
