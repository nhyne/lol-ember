import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default Controller.extend({
  match: computed(function player() {
    return this.get('model');
  })
});
